exports.config = {
  namespace: 'mycomponent',
  outputTargets: [
    {
      type: 'dist'
    },
    {
      type: 'www',
      serviceWorker: false
    }
  ],
  // plugins: [
  //   sass({
  //     injectGlobalPaths: [
  //       'src/scss/_global.scss'
  //     ]
  //   })
  // ]
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
}
