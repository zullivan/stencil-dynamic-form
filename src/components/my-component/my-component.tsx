import { Component, Prop } from '@stencil/core';

@Component({
  tag: 'my-component',
  styleUrl: 'my-component.css',
  shadow: true
})
export class MyComponent {

  @Prop() title: string;
  @Prop() subtitle: string;

  render() {
    return (
      <div>
        <h1>{this.title}</h1><br />{this.subtitle}
      </div>
    );
  }
}
