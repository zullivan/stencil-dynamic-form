import { Component, Prop, State, Listen, Event, EventEmitter } from '@stencil/core';

export interface FormElement {
    component: string;
    props: any;
};

const myForm: FormElement[] = [
    {
        component: 'input',
        props: {
            value: 'ZOL',
            name: 'nama'
        }
    },
    {
        component: 'input',
        props: {
            value: 'ZOL',
            name: 'alamat'
        }
    },
    {
        component: 'input',
        props: {
            type: 'number',
            value: 5,
            step: 1,
            name: 'no'
        }
    },
    {
        component: 'select',
        props: {
            options: [{ id: 'tes', name: 'TES' }],
            value: 5,
            step: 1
        }
    }
];

@Component({
    tag: 'some-form',
    styleUrl: 'some-form.css',
    shadow: true
})
export class SomeForm {

    @Prop() inputs: FormElement[];

    @State() open: boolean;
    @State() value: object = {};

    @Prop() getValue: any = this.value;

    @Listen('Change')

    @Event() getValues: EventEmitter;

    handleChange(event) {
        this.value[event.target.name] = event.target.value;
        this.getValues.emit(this.value)

        if (event.target.validity.typeMismatch) {
            console.log('this element is not valid')
        }
    }

    render() {
        return this.inputs.map((fe: FormElement) => {
            // OPTION 1: with container component
            // return <dynamic-container component={fe.component} props={...fe.props}></dynamic-container>

            // OPTION 2: without container component
            //return h(fe.component, { ...fe.props });

            if (fe.component === 'select') {
                return <div class="form-group">
                    <label>{fe.label}</label>
                    <fe.component {...fe.props} onInput={(e) => this.handleChange(e)} value={this.value[fe.props.name]} class="form-control">
                        {fe.props.options.map((v) => {
                            return (<option value={v.id}>{v.name}</option>)
                        })}
                    </fe.component>
                </div>
            } else {
                return <div class="form-group">
                    <label>{fe.label}</label>
                    <fe.component {...fe.props} onInput={(e) => this.handleChange(e)} value={this.value[fe.props.name]} class="form-control" />
                </div>
            }
        });
    }
}
